<?php

define("DSN", "mysql:host=localhost;dbname=gsb;charset=UTF8");
define("USER", "root");
define("PASSWORD","root");

spl_autoload_register(function ($class) {
    include $_SERVER['DOCUMENT_ROOT'].'/gsb-tp/class/'.$class.'.php';
});

session_start();
$explodeUrl = explode('/', $_SERVER['PHP_SELF']);

if (!$explodeUrl[count($explodeUrl) - 1] == "index.php") {
    if (!isset($_SESSION["connectedUser"])) {
        header("Location:index.php");
    }
}
