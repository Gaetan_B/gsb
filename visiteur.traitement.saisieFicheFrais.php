<?php

require_once './conf/config.php';

if (isset($_REQUEST) && isset($_SESSION["ficheFraisCourante"])) {
       $ficheFraisCourante = $_SESSION["ficheFraisCourante"] ;
    foreach ($_REQUEST as $idFrais => $quantite) {
        $ligneFraisForfait = $ficheFraisCourante->getLigneFraisForfait($idFrais);
        $ligneFraisForfait->setQuantite($quantite);
    }
    $ficheFraisCourante->save();
}

header("Location: visiteur.saisieFicheFrais.php");

