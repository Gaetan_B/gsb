<?php require_once 'conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
    <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

            <?php include_once 'comptable.menu.inc.php'; ?>

            <?php
            $idFicheFrais = $_REQUEST["idFicheFrais"];
            $ficheFrais = FicheFrais::fetch($idFicheFrais);
            $collectionLigneFraisForfait = $ficheFrais->getCollectionLigneFraisForfait();
            ?>
            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1>
                    <?php echo $ficheFrais->getVisiteur()->getPrenom()                ?> &nbsp; 
                    <?php echo $ficheFrais->getVisiteur()->getNom() ?>
                </h1>
                <h3><?php echo $ficheFrais->getEtat()->getLibelleEtat() ?></h3>
                <p class="lead">
                <form method="post" action="comptable.traitement.modificationFicheFrais.php">
                    
                    <?php
                    foreach ($collectionLigneFraisForfait as $ligneFraisForfait):
                        ?>
                        <div class="col-sm-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
    <?php
    echo $ligneFraisForfait->getFraisForfait()->getLibelleFraisForfait();
    ?>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        <input type="number" id="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                               name="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
    <?php if ($ligneFraisForfait->getFraisForfait()->getIdFraisForfait() == "NUI"): ?>
                                                   min="0" max="31"
                                               <?php endif; ?>
                                               pattern="[0-9]+"

                                               value="<?php
                                           echo $ligneFraisForfait->getQuantite();
                                           ?>"
                                               />
                                    </p>
                                    <p>
    <?php //echo $fraisForfaitCourant->montantFicheFrais;  ?>
                                    </p>
                                </div>
                            </div>
                        </div>
    <?php
endforeach;
?>

                    
<input type="hidden" name="idFicheFrais" value="<?php echo $ficheFrais->getIdFicheFrais(); ?>" />
       <button type="submit" class="btn btn-success">Valider</button>
                </form>
                </p>

            </div>



            <!-- Site footer -->
            <footer class="footer">
                <p>&copy; GSB 2015</p>
            </footer>

        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>






