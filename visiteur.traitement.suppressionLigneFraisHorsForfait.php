<?php

require_once './conf/config.php';

if (isset($_REQUEST["idLigneFraisHorsForfait"]) && isset($_REQUEST["idFicheFrais"])) {
    $idLigneFraisHorsForfait = $_REQUEST["idLigneFraisHorsForfait"];    
    if (isset($_SESSION["ficheFraisCourante"])) {
        $ficheFrais = $_SESSION["ficheFraisCourante"];
    } else {
        $ficheFrais = FicheFrais::fetch($idFicheFrais);
    }
    $ficheFrais->removeLigneFraisHorsForfait($idLigneFraisHorsForfait);
    
}
header("location: visiteur.saisieHorsForfait.php");