<?php require_once './conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
    <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

             <?php include_once 'visiteur.menu.inc.php'; ?>


            <h1>Consultation des fiches de frais</h1>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <?php
                    $visiteurCourant = $_SESSION["connectedUser"];
                    $collectionFicheFrais = $visiteurCourant->getCollectionFicheFrais();
                    if ($collectionFicheFrais != null):
                        foreach ($collectionFicheFrais as $ficheFrais):
                            ?>
                    
                            <div class=" col-md-5">
                                <div class="list-group">
                                    <a href="" class="list-group-item active">
                                        <h4 class="list-group-item-heading">
                                            Fiche de frais de :
                                            <?php echo $ficheFrais->getMoisAnnee(); ?>
                                        </h4>
                                        <p class="list-group-item-text">
                                            <?php
                                            echo $ficheFrais->getEtat()->getLibelleEtat();
                                            ;
                                            ?>
                                        </p>
                                    </a>
                                    <?php
                                    if ($ficheFrais->getCollectionLigneFraisForfait() != null):
                                        foreach ($ficheFrais->getCollectionLigneFraisForfait() as $ligneFraisForfait) :
                                            ?>
                                            <a href="#" class="list-group-item">
                                                <h4 class="list-group-item-heading">
                                                    <?php
                                                    echo $ligneFraisForfait->getfraisForfait()->getLibelleFraisForfait();
                                                    ?>
                                                </h4>
                                                <p class="list-group-item-text">
                                                    <?php echo $q = $ligneFraisForfait->getQuantite(); ?>
                                                    <span>*</span>
                                                    <span>(
                                                        <?php echo $m = $ligneFraisForfait->getfraisForfait()->getMontantFraisForfait(); ?> 
                                                        )</span>
                                                    <span>
                                                        total : 
                                                        <?php
                                                        echo $q * $m;
                                                        ?>
                                                        €</span>

                                                </p>

                                            </a>

                                            <?php
                                        endforeach;
                                    endif;
                                    ?>

                                    <a class="btn btn-success col-md-12 " href="visiteur.consulterFraisHorsForfait.php?idFicheFrais=<?php echo $ficheFrais->getIdFicheFrais(); ?>" role="button">Frais hors forfait »</a>
                                    <br /><br />
                                </div> 

                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>




                </div>
            </div>
            
        </div>



        <!-- Site footer -->
        <footer class="footer">
            <p>&copy; GSB 2015</p>
        </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

