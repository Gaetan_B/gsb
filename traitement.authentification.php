<?php

require_once  'conf/config.php';

$login = $_POST["login"];
$mdp = $_POST["mdp"];

$pdo = new PDO(DSN,USER,PASSWORD);
$dbAuthentification = new DataBaseAuthentification($pdo);

if ($dbAuthentification->check($login, $mdp)) {
    if ($_SESSION["connectedUser"] instanceof Visiteur) {
        header("Location: visiteur.accueil.php");
    } else if ($_SESSION["connectedUser"] instanceof Comptable) {
        header("Location: comptable.accueil.php");
    }
} else {
    header('Location: index.php?authentification="Erreur"');
}
    