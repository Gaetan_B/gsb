# Application Appli-Frais [V2.0] #

## Architecture: LAMP ##

## Arborescence de l'application ##

    [tree-AppliFrais]
        |
        |- [DIR] bootstrap
        |- [DIR] class (domain class)
        |- [DIR] documentation (API) #ApiGen#
        |- [DIR] config
        |    |----gsb.sql (script sql pour installer la BD)
        |    |----config.php (fichier de configuration de l'application)
        |    |----phpdoc.dist.xml (fichier pour la génération de documentation phpDocumentor)
        |
        |-- index.php
        |-- [autres fichiers de l'application]
        |-- comptable.x
        |-- visiteur.x

# *Installation* #

1. cloner le dépot dans le répertoire de publication (/var/www/html/)
2. installer la bd à partir du script présent dans le répertoire conf
3. modifier (si nécessaire) le fichier /conf/config.php 
    [les paramètres d'accès à la BD]

# *Configuration d'un hôte virtuel* #

1. Ajouter une entrée dans le fichier */etc/hosts*

```
#!/etc/hosts

127.0.0.1     gsb
```


2. Définir un hôte virtuel dans le fichier */etc/apache2/sites-available/gsb.conf*

```
#!/etc/apache2/sites-available/gsb.conf

<VirtualHost *:80>
        ServerName gsb
        DocumentRoot /var/www/html/gsb

        <Directory /var/www/html/gsb>
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
```


3. exécuter les commandes suivantes :


```
#!shell

cd /etc/apache2/sites-available/
sudo a2ensite gsb.conf
sudo service apache2 reload 
```


# *Divers* #

Information de connexion
visiteur : dandre ; oppg5
comptable : garnaud ; Azerty1

La documentation a été généré à l'aide de l'application ApiGen (http://www.apigen.org/)

PhpDocumentor ne fonctionne pas correctement avec php7
Le fichier de configuration de PhpDocumentor est défini, pour une utilisation ultérieur, dans le répertoire config
