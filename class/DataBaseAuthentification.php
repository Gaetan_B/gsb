<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Authentification via une Db
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 1.0
 * @category Technical class
 * 
 */
class DataBaseAuthentification implements IAuthentification {

   
    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function check($user, $password, $options = null) {
        $reussi = false;

        $pdoStatement = $this->pdo->prepare(DataBaseAuthentification::$requeteCredentialGeneric);
        $pdoStatement->bindParam(":login", $user);
        $pdoStatement->bindParam(":mdp", $password);
        $pdoStatement->execute();
        if ($pdoStatement->rowCount() != 0) {
            $record = $pdoStatement->fetch();
            $profil = $record["profil"];
            switch ($profil) {
                case "Visiteur":
                    $_SESSION["connectedUser"] = Visiteur::fetch($record["idUser"]);
                    break;
                case "Comptable" :
                    $_SESSION["connectedUser"] = Comptable::fetch($record["idUser"]);
                    break;                    
                default:                                    
                    throw new Exception("PROFIL INEXISTANT");
            }

            $reussi = true;
        }

        return $reussi;
    }

    protected $pdo;
    private static $requeteCredentialGeneric = "select * from user where login=:login and mdp=:mdp";

}
