<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente un comptable
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class Comptable extends AbstractUser {

    /**
     * Retourne un comptable à partir d'un identifiant
     * 
     * @param String $idUser un identifiant utilisateur
     * @return Comptable|null Le comptable dont l'identifiant est transmis en paramètre, null si l'identifiant ne correspond à aucun comptable.
     */
    public static function fetch($idUser) {
        $comptable = null;
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(AbstractUser::$selectById);
        $pdoStatement->bindParam(':idUser', $idUser);
        if (!$pdoStatement->execute()) {            
            DataBaseAccess::errorHandling($pdoStatement->errorInfo());
        } else {
            $record = $pdoStatement->fetch();
            if ($record != false) {
                $comptable = Comptable::arrayToComptable($record);
            }
        }
        unset($pdo);

        return $comptable;
    }

    private static function arrayToComptable(array $array) {
        $c = new Comptable();
        $c->idUser = $array["idUser"];
        $c->nom = $array["nom"];
        $c->prenom = $array["prenom"];
        $c->adresse = $array["adresse"];
        $c->ville = $array["ville"];
        $c->cp = $array["cp"];
        $c->dateEmbauche = $array["dateEmbauche"];
        $c->login = $array["login"];
        $c->mdp = $array["mdp"];
        return $c;
    }

}
