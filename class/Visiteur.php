<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente un visiteur médical
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class Visiteur extends AbstractUser {

    /**
     * Obtient la collection de fiche de frais du visiteur
     * 
     * @return Array|null Un tableau associatif contenant les fiches de frais du visiteur, null en cas d'absence de fiche de frais
     */
    public function getCollectionFicheFrais() {
        return $this->collectionFicheFrais;
    }

    /**
     * Retourne la collection des visiteurs médicaux
     * 
     * @return Array La collection des visiteurs médicaux
     */
    public static function fetchAll() {
        $collectionVisiteurs = array();
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$select);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionVisiteurs[] = Visiteur::arrayToVisiteur($record);
        }
        unset($pdo);
        return $collectionVisiteurs;
    }

    /**
     * Obtient une collection de visiteurs qui possède au moins une fiche de frais dont l'état correspond à celui transmis en paramètre 
     * 
     * @param String $libelleEtat
     * @return Array Une collection de visiteurs 
     */
    public static function fetchAllByEtat($libelleEtat) {
        $collectionVisiteurs = array();
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$selectByEtat);
        $pdoStatement->bindParam(":etat", $libelleEtat);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionVisiteurs[] = Visiteur::arrayToVisiteur($record);
        }
        unset($pdo);
        return $collectionVisiteurs;
    }

    /**
     * Retourne un visiteur à partir d'un identifiant
     * 
     * @param String $idUser
     * @return Visiteur|null Le visiteur dont l'identifiant est transmis en paramètre, null si l'identifiant ne correspond à aucun visiteur.
     */
    public static function fetch($idUser) {
        $visiteur = null;
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$selectById);
        $pdoStatement->bindParam(':idUser', $idUser);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch();
        if ($record != false) {
            $visiteur = Visiteur::arrayToVisiteur($record);
        }
        unset($pdo);
        return $visiteur;
    }

    /**
     * Sauvegarde le visiteur
     */
    public function save() {
        if ($this->idUser != null) {
            $this->update();
        } else {
            $this->insert();
        }
        $this->saveCollectionFicheFrais();
    }

    /**
     * Supprime le visiteur
     */
    public function delete() {
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$delete);
        $pdoStatement->bindParam(':idUser', $this->idUser);
        $pdoStatement->execute();
        unset($pdo);
    }

    /**
     * Initialise une fiche de frais pour le mois et l'année concernée
     * Si la fiche frais existe déja, celle-ci est retournée
     * 
     * @param String $moisAnnee le mois et l'annee de la fiche de frais
     * @param String $nbJustificatif le nombre de justificatif
     * @return FicheFrais la fiche frais pour le mois et l'année transmis
     */
    public function creerFicheFrais($moisAnnee, $nbJustificatif = null) {
        $ficheFrais = null;
        if (!$this->existe($moisAnnee)) {
            $ficheFrais = new FicheFrais($this);
            $ficheFrais->setMoisAnnee($moisAnnee);
            $ficheFrais->getNbJustificatifs($nbJustificatif);
            $this->collectionFicheFrais[] = $ficheFrais;
        } else {
            $ficheFrais = $this->getFicheFrais($moisAnnee);
        }
        return $ficheFrais;
    }

    /**
     * retourne une fiche de frais pour le mois et l'année concernée
     * 
     * @param String $moisAnnee le mois et l'annee de la fiche de frais
     * @return FicheFrais|null Une fiche de frais, ou null si elle n'est pas référencée dans la collection
     */
    public function getFicheFrais($moisAnnee) {
        if (is_array($this->collectionFicheFrais)) {
            foreach ($this->collectionFicheFrais as $ficheFrais) {
                if ($ficheFrais->getMoisAnnee() == $moisAnnee) {
                    return $ficheFrais;
                }
            }
        }
        return null;
    }

    /**
     * retourne une fiche de frais à partir d'un identifiant de fiche de frais
     * 
     * @param String $idFicheFrais Un identifiant de fiche de frais
     * @return FicheFrais|null Une fiche de frais, ou null si elle n'est pas référencée dans la collection
     */
    public function getFicheFraisById($idFicheFrais) {
        if (is_array($this->collectionFicheFrais)) {
            foreach ($this->collectionFicheFrais as $ficheFrais) {
                if ($ficheFrais->getIdFicheFrais() == $idFicheFrais) {
                    return $ficheFrais;
                }
            }
        }
        return null;
    }

    /**
     * retourne une fiche de frais à partir d'un libelle d'état
     * 
     * @param String $libelleEtat Le libellé d'un état
     * @return FicheFrais|null Une fiche de frais, ou null si elle n'est pas référencée dans la collection
     */
    public function getFicheFraisByEtat($libelleEtat) {
        if (is_array($this->collectionFicheFrais)) {
            foreach ($this->collectionFicheFrais as $ficheFrais) {
                if ($ficheFrais->getEtat()->getIdEtat() == $libelleEtat) {
                    return $ficheFrais;
                }
            }
        }
        return null;
    }

    /**
     * Teste l'existance d'une fiche de frais dans la collection
     * 
     * @param String $moisAnnee le mois et l'annee de la fiche de frais
     * @return Boolean True si elle existe, false dans le cas contraire
     */
    public function existe($moisAnnee) {
        if (is_array($this->collectionFicheFrais)) {
            foreach ($this->collectionFicheFrais as $ficheFrais) {
                if ($ficheFrais->getMoisAnnee() == $moisAnnee) {
                    return true;
                }
            }
        }
        return false;
    }

    private $collectionFicheFrais;
    
    protected static $select = "select * from user where profil = 'Visiteur'";
    protected static $selectByEtat = "select distinct(user.idUser),user.* from user,fichefrais where fichefrais.idVisiteur = user.idUser and profil = 'Visiteur' and idEtat=:etat";

    private function update() {
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$udpate);
        $pdoStatement->bindParam(':id', $this->idUser);
        $pdoStatement->bindParam(':nom', $this->nom);
        $pdoStatement->bindParam(':prenom', $this->prenom);
        $pdoStatement->bindParam(':adresse', $this->adresse);
        $pdoStatement->bindParam(':ville', $this->ville);
        $pdoStatement->bindParam(':cp', $this->cp);
        $pdoStatement->bindParam(':dateEmbauche', $this->dateEmbauche);
        $pdoStatement->bindParam(':login', $this->login);
        $pdoStatement->bindParam(':mdp', $this->mdp);
        $pdoStatement->execute();
        unset($pdo);
    }

    private function insert() {
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(Visiteur::$insert);
        $pdoStatement->bindParam(':nom', $this->nom);
        $pdoStatement->bindParam(':prenom', $this->prenom);
        $pdoStatement->bindParam(':adresse', $this->adresse);
        $pdoStatement->bindParam(':ville', $this->ville);
        $pdoStatement->bindParam(':cp', $this->cp);
        $pdoStatement->bindParam(':dateEmbauche', $this->dateEmbauche);
        $pdoStatement->bindParam(':login', $this->login);
        $pdoStatement->bindParam(':mdp', $this->mdp);
        $resultat = $pdoStatement->execute();
        $nbLignesAffectees = $pdoStatement->rowCount();
        if ($resultat != false || $nbLignesAffectees == 1) {
            $lastId = $pdo->lastInsertId();
            $this->idUser = $lastId;
        }
        unset($pdo);
    }

    private function saveCollectionFicheFrais() {
        foreach ($this->collectionFicheFrais as $ficheFrais) {
            $ficheFrais->save();
        }
    }

    private static function arrayToVisiteur(Array $array) {
        $v = new Visiteur();
        $v->idUser = $array["idUser"];
        $v->nom = $array["nom"];
        $v->prenom = $array["prenom"];
        $v->adresse = $array["adresse"];
        $v->ville = $array["ville"];
        $v->cp = $array["cp"];
        $v->dateEmbauche = $array["dateEmbauche"];
        $v->login = $array["login"];
        $v->mdp = $array["mdp"];
        $v->collectionFicheFrais = FicheFrais::fetchAllVisiteur($v);
        return $v;
    }

}
