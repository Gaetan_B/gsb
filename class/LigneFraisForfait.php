<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente la ligne de frais d'une fiche de frais
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class LigneFraisForfait {

    private $ficheFrais;
    private $fraisForfait;
    private $quantite;
    private static $selectByIdFicheFrais = 'select * from lignefraisforfait where idFicheFrais=:idFicheFrais';
    private static $insert = 'insert into lignefraisforfait (idFicheFrais,idFraisForfait,quantite)values(:idFicheFrais,:idFraisForfait,:quantite)';
    private static $update = 'update lignefraisforfait set quantite=:quantite where idFicheFrais=:idFicheFrais and idFraisForfait=:idFraisForfait ';
    private static $selectById = 'select * from lignefraisforfait where idFicheFrais=:idFicheFrais and idFraisForfait=:idFraisForfait';

    private function existe() {
        $existe = false;
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisForfait::$selectById);
        $idFraisForfait = $this->fraisForfait->getIdFraisForfait();
        $idFicheFrais = $this->ficheFrais->getIdFicheFrais();
        $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais);
        $pdoStatement->bindParam(':idFraisForfait', $idFraisForfait);
        $pdoStatement->execute();
        if ($pdoStatement->rowCount() == 1) {
            $existe = true;
        }
        unset($pdo);
        return $existe;
    }

    private function insert() {
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisForfait::$insert);
        $idFraisForfait = $this->fraisForfait->getIdFraisForfait();
        $idFicheFrais = $this->ficheFrais->getIdFicheFrais();
        $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais, PDO::PARAM_INT);
        $pdoStatement->bindParam(':idFraisForfait', $idFraisForfait, PDO::PARAM_STR);
        $pdoStatement->bindParam(':quantite', $this->quantite, PDO::PARAM_INT);
        $resultat = $pdoStatement->execute();        
        unset($pdo);
    }

    private function update() {
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisForfait::$update);
        $idFraisForfait = $this->fraisForfait->getIdFraisForfait();
        $idFicheFrais = $this->ficheFrais->getIdFicheFrais();
        $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais);
        $pdoStatement->bindParam(':idFraisForfait', $idFraisForfait);
        $pdoStatement->bindParam(':quantite', $this->quantite);
        $resultat = $pdoStatement->execute();        
        unset($pdo);
    }

    private static function arrayToLigneFraisForfait(Array $array, FicheFrais $ficheFrais = null, FraisForfait $fraisForfait = null) {
        if ($ficheFrais == null) {
            $ficheFrais = FicheFrais::fetch($array["idFicheFrais"]);
        }
        if ($fraisForfait == null) {
            $fraisForfait = FraisForfait::fetch($array["idFraisForfait"]);
        }
        $quantite = $array["quantite"];
        $ligneFraisForfait = new LigneFraisForfait($ficheFrais, $fraisForfait, $quantite);
        return $ligneFraisForfait;
    }

    /**
     * Obtient les lignes de frais forfait d'une fiche de frais
     * 
     * @param FicheFrais $ficheFrais une fiche de frais
     * @return Array
     */
    public static function fetchAllByFicheFrais(FicheFrais $ficheFrais) {
        $ligneFicheFrais = array();
        $pdo = new PDO(DSN, USER,PASSWORD);
        if ($pdo != null) {
            $pdoStatement = $pdo->prepare(LigneFraisForfait::$selectByIdFicheFrais);
            $idFicheFrais = $ficheFrais->getIdFicheFrais();
            $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais);
            $pdoStatement->execute();
            $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
            foreach ($recordSet as $record) {
                $ligneFicheFrais[] = LigneFraisForfait::arrayToLigneFraisForfait($record, $ficheFrais);
            }
        }
        return $ligneFicheFrais;
    }

    /**
     * Sauvegarde une ligne de frais forfait
     */
    public function save() {
        if ($this->existe() == false) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    /**
     * Compare deux lignes de frais forfait à partir de leur identifiant
     * 
     * @param LigneFraisForfait $lff1 une ligne de frais forfait
     * @param LigneFraisForfait $lff2 une ligne de frais forfait
     * @return boolean true en cas d'égalité, false dans le cas contraire
     */
    public static function equals(LigneFraisForfait $lff1, LigneFraisForfait $lff2) {
        $equals = false;
        if ($lff1->ficheFrais instanceof FicheFrais && $lff2->ficheFrais instanceof FicheFrais) {
            if ($lff1->ficheFrais->getIdFicheFrais() == $lff2->ficheFrais->getIdFicheFrais()) {
                $equals = true;
            }
        }
        return false;
    }

    /**
     * Initialise une ligne de frais forfait à partir d'une fiche de frais et d'un frais forfait
     * 
     * @param FicheFrais $ficheFrais Une fiche de frais
     * @param FraisForfait $fraisForfait Un frais forfait
     * @param string|float $quantite la quantite de frais forfait
     * @return \LigneFraisForfait une ligne de frais forfait
     */
    public function __construct(FicheFrais $ficheFrais, FraisForfait $fraisForfait, $quantite) {
        $this->ficheFrais = $ficheFrais;
        $this->quantite = $quantite;
        $this->fraisForfait = $fraisForfait;
    }

    /**
     * Retourne le type de frais forfait de la ligne de frais courante
     * 
     * @return \FraisForfait Un frais forfait
     */
    public function getFraisForfait() {
        return $this->fraisForfait;
    }

    /**
     * définit la quantité pour une ligne de frais forfait
     * 
     * @param numeric $quantite
     * @throws Exception ArgumentException
     */
    public function setQuantite($quantite) {
        if (is_numeric($quantite) == false) {
            throw new Exception("Argument Type Exception");
        }
        $this->quantite = $quantite;
    }

    /**
     * Obtient la quantité associé à une ligne de frais forfait
     * 
     * @return string|float
     */
    public function getQuantite() {
        return $this->quantite;
    }

}
