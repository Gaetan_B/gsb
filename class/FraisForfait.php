<?php

/**
 * Application <Appli-Frais>
 * 
 */

/**
 * Représente un frais forfaitaire
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class FraisForfait {

    /**
     * Obtient l'identifiant du frais forfait
     * 
     * @return string
     */
    public function getIdFraisForfait() {
        return $this->idFraisForfait;
    }

    /**
     * obtient le libellé du frais forfait
     * 
     * @return string
     */
    public function getLibelleFraisForfait() {
        return $this->libelleFraisForfait;
    }

    /**
     * Obtient le montant du frais forfait
     * 
     * @return string|float 
     */
    public function getMontantFraisForfait() {
        return $this->montantFraisForfait;
    }

    /**
     * Retourne le frais forfait correspondant à l'identifiant transmis en paramètre
     * 
     * @param string $idFraisForfait l'identifiant du frais forfait
     * @return FraisForfait|null Un frais forfait ou null si l'identifiant n'existe pas
     */
    public static function fetch($idFraisForfait) {
        $fraisForfait = new FraisForfait();
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->prepare(FraisForfait::$selectById);
        $pdoStatement->bindParam(':idFraisForfait', $idFraisForfait);
        $resultat = $pdoStatement->execute();        
            $record = $pdoStatement->fetch();
            if ($record != false) {
                $fraisForfait = FraisForfait::arrayToFraisForfait($record);
            }            
        unset($pdo);
        return $fraisForfait;
    }

    /**
     * Obtient les types de frais forfait référencés
     * 
     * @return Array Une collection des types de frais forfait
     */
    public static function fetchAll() {
        $ff = array();
        $pdo = new PDO(DSN, USER,PASSWORD);
        $pdoStatement = $pdo->query(FraisForfait::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $ff[] = FraisForfait::arrayToFraisForfait($record);
        }
        unset($pdo);
        return $ff;
    }

    private static function arrayToFraisForfait(Array $recordSetFraisForfait) {
        $ff = new FraisForfait();
        $ff->idFraisForfait = $recordSetFraisForfait["idFraisForfait"];
        $ff->libelleFraisForfait = $recordSetFraisForfait["libelleFraisForfait"];
        $ff->montantFraisForfait = $recordSetFraisForfait["montantFraisForfait"];
        return $ff;
    }

    private $idFraisForfait;
    private $libelleFraisForfait;
    private $montantFraisForfait;
    private static $select = "select * from fraisforfait";
    private static $selectById = "select * from fraisforfait where idFraisForfait=:idFraisForfait";

}
