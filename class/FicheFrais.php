<?php

/**
 * Application <Appli-Frais>
 *  
 */

/**
 * Représente une fiche de frais
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 * 
 */
class FicheFrais {

    /**
     * Obtient le mois et l'année de la fiche de frais
     * 
     * @return String Le mois et la date de la fiche de frais     */
    public function getMoisAnnee() {
        return $this->moisAnnee;
    }

    /**
     * Modifie le mois et l'année de la fiche de frais
     * 
     * @param String $moisAnnee
     */
    public function setMoisAnnee($moisAnnee) {
        $this->moisAnnee = $moisAnnee;
    }

    /**
     * Initialise une ligne de frais hors forfait à la fiche de frais courante
     * 
     * @param string $libelle le libellé de la ligne de frais hors forfait
     * @param DateTime $date la date de la ligne de frais hors forfait 
     * @param string $montant le montant de la ligne de frais hors forfait
     */
    public function setLigneFraisHorsForfait($libelle, DateTime $date, $montant) {
        $ligneFraisHorsForfait = new LigneFraisHorsForfait($this);
        $ligneFraisHorsForfait->setDate($date);
        $ligneFraisHorsForfait->setLibelle($libelle);
        $ligneFraisHorsForfait->setMontant($montant);
        $this->collectionLigneFraisHorsForfait[] = $ligneFraisHorsForfait;
    }

    /**
     * Obtient une ligne de frais hors forfait à partir de son identifiant
     * 
     * @param string $idFraisHorsForfait un identifiant de frais hors forfait
     * @return LigneDeFraisHorsForfait|null une ligne de frais hors forfait à partir d'un identifiant ou null si celui-ci ne référence aucune ligne de frais hors forfait
     */
    public function getLigneFraisHorsForfait($idFraisHorsForfait) {
        foreach ($this->collectionLigneFraisHorsForfait as $ligneFraisHorsForfait) {
            if ($ligneFraisHorsForfait->getIdLigneFraisHorsForfait() == $idFraisHorsForfait) {
                return $ligneFraisHorsForfait;
            }
        }
        return null;
    }

    /**
     * Obtient les lignes de frais hors forfait d'une fiche de frais
     * 
     * @return Array
     */
    public function getCollectionLigneFraisHorsForfait() {
        return $this->collectionLigneFraisHorsForfait;
    }

    /**
     * Retourne le visiteur lié à la fiche de frais
     * 
     * @return Visiteur
     */
    public function getVisiteur() {
        return $this->visiteur;
    }

    /**
     * Obtient les lignes de frais forfait
     * 
     * @return Array
     */
    public function getCollectionLigneFraisForfait() {
        return $this->collectionLigneFraisForfait;
    }

    /**
     * Supprime une ligne de frais hors forfait à partir de son emplacement dans la collection de frais hors forfait
     * 
     * @param string|int $indice
     */
    public function removeLigneFraisHorsForfait($idLigneFraisHorsForfait) {
        if (is_array($this->collectionLigneFraisHorsForfait)) {
            foreach ($this->collectionLigneFraisHorsForfait as $lfhf) {
                if ($lfhf->getIdLigneFraisHorsForfait() == $idLigneFraisHorsForfait) {
                    $lfhf->delete();
                    $indice = key($this->collectionLigneFraisHorsForfait);
                    unset($this->collectionLigneFraisHorsForfait[$indice]);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Obtient l'identifiant de la fiche de frais
     * 
     * @return string
     */
    public function getIdFicheFrais() {
        return $this->idFicheFrais;
    }

    /**
     * Obtient le nombre de justificatifs
     * 
     * @return string|int le nombre de justificatifs
     */
    public function getNbJustificatifs() {
        return $this->nbJustificatifs;
    }

    /**
     * Obtient le montant valide
     * 
     * @return string|float
     */
    public function getMontantValide() {
        return $this->montantValide;
    }

    /**
     * Obtient la date de la dernière modification de la fiche de frais
     * 
     * @return DateTime
     */
    public function getDateModif() {
        return $this->dateModif;
    }

    /**
     * Obtient l'état de la fiche de frais
     * 
     * @return Etat l'état de la fiche de frais
     */
    public function getEtat() {
        return $this->etat;
    }

    /**
     * Modifie l'état d'une fiche de frais
     * 
     * @param Etat $unEtat le nouvel état de la fiche de frais
     */
    public function setEtat(Etat $unEtat) {
        $this->etat = $unEtat;
    }

    /**
     * Ajoute une ligne de frais forfait à la fiche de frais
     * 
     * @param LigneFraisForfait $ligneFraisForfait une ligne de frais forfait
     */
    public function setLigneFraisForfait(LigneFraisForfait $ligneFraisForfait) {
        $exist = false;
        foreach ($this->collectionLigneFraisForfait as $ligneFraisForfaitCourante) {
            if (LigneFraisForfait::equals($ligneFraisForfait, $ligneFraisForfaitCourante)) {
                $exist = true;
                break;
            }
        }
        if ($exist == false) {
            $this->collectionLigneFraisForfait[] = $ligneFraisForfait;
        }
    }

    /**
     * Obtient une ligne de frais forfait à partir d'un identfiant de frais forfait
     * 
     * @param string $idFraisForfait l'identifiant d'un frais forfait
     * @return LigneFraisForfait une ligne de frais forfait
     */
    public function getLigneFraisForfait($idFraisForfait) {
        if (is_array($this->collectionLigneFraisForfait)) {
            foreach ($this->collectionLigneFraisForfait as $ligneFraisForfait) {
                if ($ligneFraisForfait->getFraisForfait()->getIdFraisForfait() == $idFraisForfait) {
                    return $ligneFraisForfait;
                }
            }
        }
        return null;
    }

    /**
     * Initialise une fiche frais
     * 
     * @param Visiteur $visiteur Un visiteur
     */
    public function __construct(Visiteur $visiteur) {
        $this->collectionLigneFraisForfait = array();
        $collectionFraisForfait = FraisForfait::fetchAll();
        if (is_array($collectionFraisForfait)) {
            foreach ($collectionFraisForfait as $fraisForfaitCourant) {
                $ligneFraisForfait = new LigneFraisForfait($this, $fraisForfaitCourant, 0);
                $this->collectionLigneFraisForfait[] = $ligneFraisForfait;
            }
        }
        $this->etat = Etat::fetch('EC');
        $this->visiteur = $visiteur;
    }

    /**
     * Sauvegarde la fiche de frais courante
     */
    public function save() {
        if ($this->idFicheFrais == null) {
            $this->insert();
        } else {
            $this->update();
        }
        $this->saveLigneFicheFraisForfait();
        $this->saveLigneFraisHorsForfait();
    }

    /**
     * Supprime la fiche de frais courante
     */
    public function delete() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(FicheFrais::$delete);
        $idFicheFrais = $this->idFicheFrais;
        $pdoStatement->bindParam(":idFicheFrais", $idFicheFrais);
        if (!$pdoStatement->execute()) {
            DataBaseAccess::errorHandling($pdoStatement->errorInfo());
        }
        $this->idFicheFrais = null;
        unset($pdo);
    }

    /**
     * Retourne une fiche de frais à partir d'un identifiant de fiche de frais
     * 
     * @param string $idFicheFrais l'identifiant d'une fiche de frais
     * @return FicheFrais|null une fiche de frais, null si l'identifiant n'existe pas
     */
    public static function fetch($idFicheFrais) {
        $ficheFrais = null;
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(FicheFrais::$selectByIdFicheFrais);
        $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais);
        if (!$pdoStatement->execute()) {
            DataBaseAccess::errorHandling($pdoStatement->errorInfo());
        }
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($record != false) {
            $ficheFrais = FicheFrais::arrayToFicheFrais($record);
        }
        return $ficheFrais;
    }

    /**
     * Retourne les fiches de frais d'un visiteur
     * 
     * @param Visiteur $visiteur
     * @return Array Une collection de fiche de frais associée à un visiteur 
     */
    public static function fetchAllVisiteur(Visiteur $visiteur) {
        $collectionFicheFrais = array();
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(FicheFrais::$selectByIdVisiteur);
        $idVisiteur = $visiteur->getIdUser();
        $pdoStatement->bindParam(':idUser', $idVisiteur);
        if (!$pdoStatement->execute()) {
            DataBaseAccess::errorHandling($pdoStatement->errorInfo());
        }
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionFicheFrais[] = FicheFrais::arrayToFicheFrais($record, $visiteur);
        }
        unset($pdo);

        return $collectionFicheFrais;
    }

    private static function arrayToFicheFrais(Array $array, Visiteur $visiteur = null) {
        if ($visiteur == null) {
            $visiteur = Visiteur::fetch($array["idVisiteur"]);
        }
        $ff = new FicheFrais($visiteur);
        $ff->idFicheFrais = $array["idFicheFrais"];
        $ff->moisAnnee = $array['moisAnnee'];
        $ff->nbJustificatifs = $array["nbJustificatifs"];
        $ff->montantValide = $array["montantValide"];
        $ff->dateModif = $array["dateModif"];
        $ff->etat = Etat::fetch($array["idEtat"]);
        $ff->collectionLigneFraisForfait = LigneFraisForfait::fetchAllByFicheFrais($ff);
        $ff->collectionLigneFraisHorsForfait = LigneFraisHorsForfait::fetchAllByFicheFrais($ff);
        return $ff;
    }

    private function saveLigneFicheFraisForfait() {
        foreach ($this->collectionLigneFraisForfait as $ligneFraisForfait) {
            $ligneFraisForfait->save();
        }
    }

    private function saveLigneFraisHorsForfait() {
        foreach ($this->collectionLigneFraisHorsForfait as $ligneFraisHorsForfait) {
            $ligneFraisHorsForfait->save();
        }
    }

    private function insert() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(FicheFrais::$insert);
        $idVisiteur = $this->visiteur->getIdUser();
        $moisAnnee = $this->moisAnnee;
        $idEtat = $this->etat->getIdEtat();
        $nbJustificatifs = $this->nbJustificatifs;
        $montantValide = $this->montantValide;
        $dateModif = $this->dateModif;
        $pdoStatement->bindParam(":idUser", $idVisiteur);
        $pdoStatement->bindParam(":moisAnnee", $moisAnnee);
        $pdoStatement->bindParam(":idEtat", $idEtat);
        $pdoStatement->bindParam(':nbJustificatifs', $nbJustificatifs);
        $pdoStatement->bindParam(':montantValide', $montantValide);
        $pdoStatement->bindParam(':dateModif', $dateModif);
        $pdoStatement->execute();
        $this->idFicheFrais = $pdo->lastInsertId();

        unset($pdo);
    }

    private function update() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(FicheFrais::$update);
        $idVisiteur = $this->visiteur->getIdUser();
        $moisAnnee = $this->moisAnnee;
        $idEtat = $this->etat->getIdEtat();
        $nbJustificatifs = $this->nbJustificatifs;
        $montantValide = $this->montantValide;
        $dateModif = $this->dateModif;
        $idFicheFrais = $this->getIdFicheFrais();
        $pdoStatement->bindParam(":idUser", $idVisiteur);
        $pdoStatement->bindParam(":moisAnnee", $moisAnnee);
        $pdoStatement->bindParam(":idEtat", $idEtat);
        $pdoStatement->bindParam(':nbJustificatifs', $nbJustificatifs);
        $pdoStatement->bindParam(':montantValide', $montantValide);
        $pdoStatement->bindParam(':dateModif', $dateModif);
        $pdoStatement->bindParam(":idFicheFrais", $idFicheFrais);
        $pdoStatement->execute();
        unset($pdo);
    }

    private $idFicheFrais;
    private $moisAnnee;
    private $nbJustificatifs;
    private $montantValide;
    private $dateModif;
    private $visiteur;
    private $etat;
    private $collectionLigneFraisForfait;
    private $collectionLigneFraisHorsForfait;
    private static $selectByIdFicheFrais = 'select * from fichefrais where idFicheFrais = :idFicheFrais';
    private static $selectByIdVisiteur = 'select * from fichefrais where idVisiteur = :idUser ';
    private static $delete = 'delete from fichefrais where idFicheFrais=:idFicheFrais ';
    private static $insert = 'insert into fichefrais (moisAnnee,idEtat,nbJustificatifs,montantValide,dateModif,idVisiteur) values (:moisAnnee,:idEtat,:nbJustificatifs,:montantValide,:dateModif,:idUser) ';
    private static $update = 'update fichefrais set  moisAnnee=:moisAnnee , nbJustificatifs=:nbJustificatifs,montantValide=:montantValide,dateModif=:dateModif,idEtat=:idEtat,idVisiteur=:idUser where idFicheFrais=:idFicheFrais ';

}
