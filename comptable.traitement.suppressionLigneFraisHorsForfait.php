<?php

require_once './conf/config.php';

if (isset($_REQUEST["idLigneFraisHorsForfait"])) {
    $idLigneFraisHorsForfait = $_REQUEST["idLigneFraisHorsForfait"];
    $lfhf = LigneFraisHorsForfait::fetch($idLigneFraisHorsForfait);
    $lfhf->delete();
    header("location: comptable.gestionFicheFrais.php");
}
