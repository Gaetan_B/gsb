<?php require_once './conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
   <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

             <?php include_once 'comptable.menu.inc.php'; ?>

            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1>Appli-Frais</h1>
                <p class="lead">Cette application vous permet de valider les notes de frais</p>
            </div>



            <!-- Site footer -->
            <footer class="footer">
                <p>&copy; GSB 2015</p>
            </footer>

        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
